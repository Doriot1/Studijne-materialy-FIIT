Studijne materialy FIIT
==============================================
Pre pripojenie do chat roomu kliknite na gitter obrazok pod tymto textom

[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/citruslee/Studijne-materialy-FIIT?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

Repozitar pre INFO a PKSS odbory

Thanks for contributing:

[@ok100](https://github.com/ok100) - shitload of files<br />
[@pepies](https://github.com/pepies) - additional PPI files<br />
[@bludivec](https://github.com/bludivec) - for great markdown material and also for pull requests
